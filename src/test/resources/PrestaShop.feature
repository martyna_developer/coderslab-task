Feature: PrestaShop tests

  Scenario Outline: Logged user is filling address form
    Given I am on the PrestaShop "https://prod-kurs.coderslab.pl" start page
    When I login as user "Sarna Sarnowska" witch email "qdfdtcxmxzwvjzvsdf@awdrt.org" and password "sarnasarna"
    And Go to Addresses page
    And Click button New addresses
    And Fill alias "<alias>"
    And Fill company "<company>"
    And Fill address bar "<address_bar>"
    And Fill postal code "<postal_code>"
    And Fill city "<city>"
    And Choose country "<country>"
    And Click save button
    Then Address form is filled correctly

    Examples: Address
      | alias   | company       | address_bar   | postal_code       | city          | country       |
      | Sarna   | Sarnex        | Sarnowska 21  | 01-100            | Sarnowo       | United Kingdom |


  Scenario Outline: Logged user is adding product to cart
    Given I am on the PrestaShop "https://prod-kurs.coderslab.pl" start page
    When I login as user "Sarna Sarnowska" witch email "qdfdtcxmxzwvjzvsdf@awdrt.org" and password "sarnasarna"
    And Add go product page "<product>"
    And Choose size "<size>"
    And Add "<quantity>" product items
    And Add product to cart
    And Go to checkout option
    And Choose address
    And Choose pick up in store option
    And Choose pay by check option
    And Order with an obligation to pay
    And Make screenshot of order
    Then Product is ordered correctly "<product>", "<size>", "<quantity>"

    Examples: Address
      | product                       | size        | quantity |
      | Hummingbird printed sweater   | M           | 5        |

  Scenario: Clean after test
    Given I am on the PrestaShop "https://prod-kurs.coderslab.pl" start page
    When I login as user "Sarna Sarnowska" witch email "qdfdtcxmxzwvjzvsdf@awdrt.org" and password "sarnasarna"
    And Go to Addresses page
    And Delete address
    Then Address is delete
