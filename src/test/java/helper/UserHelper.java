package helper;

import org.openqa.selenium.WebDriver;
import pages.HomePage;
import pages.LoginPage;
import pages.MyAccountPage;

public class UserHelper {

    public final WebDriver driver;

    // przypisalam sobie webdrivera
    public UserHelper(WebDriver driver) {
        this.driver = driver;
    }


//loguje sobie uzytkownika
    public MyAccountPage loginAsUser(String user, String email, String password) {
        LoginPage loginPage = new HomePage(driver).clickSignInBtn();
        loginPage.fillUserField(email);
        loginPage.fillPasswordField(password);
        MyAccountPage myAccountPage = loginPage.clickSubmitBtn();
        myAccountPage.checkIfUserIsLogged(user);
        return myAccountPage;

    }



}
