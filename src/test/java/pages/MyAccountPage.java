package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import static org.junit.Assert.assertEquals;

public class MyAccountPage extends BasePage {

    //tu sobie sprawdzam czy uzytkownik jest zalogowany

    private final String accountBtnWithUserNameClassName = "account";

    public MyAccountPage(WebDriver driver) {
        super(driver);
    }

    public void checkIfUserIsLogged(String user) {
        assertEquals(user, super.driver.findElement(By.className(this.accountBtnWithUserNameClassName)).getText());

    }

    public HomePage goToHomePage(String url) {
        driver.get(url);
        return new HomePage(super.driver);
    }


}
