package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class HomePage extends BasePage {

    private final String signInBtnXpath = "//a[@title =  'Log in to your customer account']";
    private final String addressBtnXpath = "//a[@title= 'Addresses']";


    public HomePage(WebDriver driver) {
        super(driver);
    }

    public LoginPage clickSignInBtn() {

        super.driver.findElement(By.xpath(this.signInBtnXpath)).click();
        return new LoginPage(super.driver);

    }

    public AddresseesPage clickAddressesBtn() {
        super.driver.findElement(By.xpath(this.addressBtnXpath)).click();
        return new AddresseesPage(super.driver);
    }

    public ProductPage goToProductPage(String product) {
        super.driver.findElement(By.xpath("//a[contains(text(),'"+ product +"')]")).click();
        return new ProductPage(super.driver);

    }
}
