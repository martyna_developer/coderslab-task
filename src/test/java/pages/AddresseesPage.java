package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;

public class AddresseesPage extends BasePage {

    private final String yourAddressTextXpath = "//section[@id = 'main']//h1[contains(text(), 'Your addresses')]";
    private final String createNewAddressXpath = "//span[text() = 'Create new address']";
    private final String newAddressTextXpath = "//section[@id = 'main']//h1[contains(text(), 'New address')]";
    private final String saveBtnXpath = "//button[contains(text(), 'Save')]";
    private final String updateBtn = "//span[contains(text(),'Update')]";

    private final String aliasName = "alias";
    private final String companyName = "company";
    private final String addressName = "address1";
    private final String postcodeName = "postcode";
    private final String cityName = "city";
    private final String countryName = "id_country";
    private final String countryXpath = "//select[@name='id_country']/option[2]";

    private final String deleteBtnXpath = "//span[contains(text(),'Delete')]";
    private final String addressDeleteAlertXpath = "//article[@class='alert alert-success']";


    public AddresseesPage(WebDriver driver) {
        super(driver);
    }

    public void checkIfAddressPageIsShown() {
        Assert.assertTrue(super.driver.findElement(By.xpath(this.yourAddressTextXpath)).isDisplayed());
    }

    public void clickCreateNewAddressBtn() {
        super.driver.findElement(By.xpath(this.createNewAddressXpath)).click();
    }

    public void checkIfAddressFormIsShown() {
        Assert.assertTrue(super.driver.findElement(By.xpath(this.newAddressTextXpath)).isDisplayed());
    }

    public void fillStreetField(String street) {
        super.driver.findElement(By.name(this.addressName)).sendKeys(street);
    }

    public void fillAliasField(String alias) {
        super.driver.findElement(By.name(this.aliasName)).sendKeys(alias);
    }

    public void fillCompanyField(String company) {
        super.driver.findElement(By.name(this.companyName)).sendKeys(company);
    }

    public void fillPostalCodeField(String postalCode) {
        super.driver.findElement(By.name(this.postcodeName)).sendKeys(postalCode);
    }

    public void fillCityField(String city) {
        super.driver.findElement(By.name(this.cityName)).sendKeys(city);
    }

    public void fillCountryField(String country) {
        super.driver.findElement(By.name(this.countryName)).sendKeys(country);
    }

    public void clickSaveBtn() {
        super.driver.findElement(By.xpath(saveBtnXpath)).click();
    }

    public void checkAddedAddress(String alias, String company, String address, String postcode, String city, String country) {

        Assert.assertTrue(driver.findElement(By.xpath("//article[@class = 'address']//address[contains(text(), '" + alias + "')]")).isDisplayed());

        super.driver.findElement(By.xpath(this.updateBtn)).click();

        Assert.assertEquals(driver.findElement(By.name(aliasName)).getAttribute("value"), alias);
        Assert.assertEquals(driver.findElement(By.name(companyName)).getAttribute("value"), company);
        Assert.assertEquals(driver.findElement(By.name(addressName)).getAttribute("value"), address);
        Assert.assertEquals(driver.findElement(By.name(postcodeName)).getAttribute("value"), postcode);
        Assert.assertEquals(driver.findElement(By.name(cityName)).getAttribute("value"), city);
        Assert.assertEquals(driver.findElement(By.xpath(countryXpath)).getText(), country);


    }

    public void clickDeleteBtn() {
        if(super.driver.findElement(By.xpath(this.deleteBtnXpath)).isDisplayed()) {
            super.driver.findElement(By.xpath(this.deleteBtnXpath)).click();
        }
    }

    public void checkIfAddressIsDelete() {
        Assert.assertTrue(super.driver.findElement(By.xpath(this.addressDeleteAlertXpath)).isDisplayed());
    }
}
