package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginPage extends BasePage {

    private final String emailFieldName = "email";
    private final String passwordFieldName = "password";
    private final String submitBtnId = "submit-login";


    public LoginPage(WebDriver driver) {
        super(driver);
    }

    public void fillUserField(String user) {
        super.driver.findElement(By.name(this.emailFieldName)).sendKeys(user);
    }

    public void fillPasswordField(String pass) {
        super.driver.findElement(By.name(this.passwordFieldName)).sendKeys(pass);
    }

    public MyAccountPage clickSubmitBtn() {
        super.driver.findElement(By.id(this.submitBtnId)).click();
        return new MyAccountPage(super.driver);
    }
}
