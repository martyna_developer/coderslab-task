package pages;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class CartPage extends BasePage {

    private final String checkoutBtnXpath = "//a[@class='btn btn-primary']";
    private final String addressXpath = "//span[@class='address-alias h4']";
    private final String confirmAddressBtnName = "confirm-addresses";
    private final String pickUpDeliveryXpath = "//div[@class='delivery-options']//div[1]//label[1]";
    private final String confirmDeliveryBtnName = "confirmDeliveryOption";
    private final String payByCheckBtnXpath = "//span[contains(text(),'Pay by Check')]";
    private final String approveTermsBtnXpath = "//input[@id='conditions_to_approve[terms-and-conditions]']";
    private final String orderBtnXpath = "//button[@class='btn btn-primary center-block']";
    private final String quantityNmbXpath = "//div[@class='col-xs-2']";

    public CartPage(WebDriver driver) {
        super(driver);
    }

    public void clickCheckoutBtn() {
        super.driver.findElement(By.xpath(this.checkoutBtnXpath)).click();
    }

    public void chooseAddress() {
        super.driver.findElement(By.xpath(this.addressXpath)).click();
        super.driver.findElement(By.name(this.confirmAddressBtnName)).click();
    }

    public void choosePickUpDeliveryOption() {
        super.driver.findElement(By.xpath(this.pickUpDeliveryXpath)).click();
        super.driver.findElement(By.name(this.confirmDeliveryBtnName)).click();
    }

    public void choosePayByCheckOption() {
        super.driver.findElement(By.xpath(this.payByCheckBtnXpath)).click();
        super.driver.findElement(By.xpath(this.approveTermsBtnXpath)).click();
    }

    public void clickOrderWithAnObligationToPay() {
        super.driver.findElement(By.xpath(this.orderBtnXpath)).click();
    }

    public void checkProductNameAndSize(String product, String size) {
        Assert.assertTrue(super.driver.findElement(By.xpath("//span[contains(text(),'"+ product +" - Size : "+ size +"')]")).isDisplayed());
    }

    public void checkQuantity(String quantity) {
        Assert.assertEquals(super.driver.findElement(By.xpath(this.quantityNmbXpath)).getText(), quantity);
    }
}
