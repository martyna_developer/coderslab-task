package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ProductPage extends BasePage {

    private final String quantityFieldName = "qty";
    private final String addToCartBtnXpath = "//button[@class='btn btn-primary add-to-cart']";
    private final String checkoutBtnXpath = "//a[@class='btn btn-primary']";

//tu sobie wyklikuje metody na zakupy (ilosc, rozmiar, dodanie do koszyka i checkbox)

    public ProductPage(WebDriver driver) {
        super(driver);
    }


    public void chooseSize(String size) {
        super.driver.findElement(By.xpath("//option[contains(text(),'" + size + "')]")).click();

    }

    public void addQuantity(String itemNmb) {

        WebElement quantityEl = super.driver.findElement(By.name(this.quantityFieldName));
        quantityEl.clear();
        quantityEl.sendKeys(itemNmb);

    }

    public void clickAddToCartBtn() {
        super.driver.findElement(By.xpath(this.addToCartBtnXpath)).click();
    }

    public CartPage clickCheckoutBtnOnPopup() {
        WebDriverWait wait = new WebDriverWait(driver,30);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(this.checkoutBtnXpath)));
        super.driver.findElement(By.xpath(this.checkoutBtnXpath)).click();
        return new CartPage(super.driver);

    }
}
