package stepDefinitions;

import helper.UserHelper;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.AddresseesPage;
import pages.HomePage;
import pages.MyAccountPage;

public class AddNewAddressSteps {

    private WebDriver driver;

    private HomePage homePage;
    private MyAccountPage myAccountPage;
    private AddresseesPage addresseesPage;

    private String url;

    private String addressAlias;
    private String company;
    private String street;
    private String postalCode;
    private String city;
    private String country;

//tu sobie robie w gherkinie wszystkie pola do logowania

    @Given("I am on the PrestaShop {string} start page")
    public void i_am_on_the_PrestaShop_start_page(String page) {

        this.url = page;
        this.driver = Hooks.driver;
        this.driver.get(page);
    }

    @When("I login as user {string} witch email {string} and password {string}")
    public void i_login_as_user_with_password(String user, String email, String password) {
        UserHelper userHelper = new UserHelper(this.driver);
        this.myAccountPage = userHelper.loginAsUser(user, email, password);
        this.homePage = this.myAccountPage.goToHomePage(url);
    }
    @When("Go to Addresses page")
    public void go_to_Addresses_page() {

        this.addresseesPage = this.homePage.clickAddressesBtn();
        this.addresseesPage.checkIfAddressPageIsShown();
    }

    @And("Click button New addresses")
    public void clickButtonNewAddresses() {
        this.addresseesPage.clickCreateNewAddressBtn();
        this.addresseesPage.checkIfAddressFormIsShown();
    }

    @When("Fill alias {string}")
    public void fill_alias(String alias) {
        this.addressAlias = alias;
        this.addresseesPage.fillAliasField(alias);
    }
    @When("Fill company {string}")
    public void fill_company(String company) {
        this.company = company;
        this.addresseesPage.fillCompanyField(company);

    }
    @When("Fill address bar {string}")
    public void fill_address_bar(String street) {
        this.street = street;
        this.addresseesPage.fillStreetField(street);
    }
    @When("Fill postal code {string}")
    public void fill_postal_code(String postalCode) {
        this.postalCode = postalCode;
        this.addresseesPage.fillPostalCodeField(postalCode);
    }
    @When("Fill city {string}")
    public void fill_city(String city) {
        this.city = city;
        this.addresseesPage.fillCityField(city);
    }
    @When("Choose country {string}")
    public void choose_country(String country) {
        this.country = country;
        this.addresseesPage.fillCountryField(country);
    }
    @When("Click save button")
    public void click_save_button() {
        this.addresseesPage.clickSaveBtn();
    }
    @Then("Address form is filled correctly")
    public void address_form_is_filled_correctly() {
        addresseesPage.checkAddedAddress(this.addressAlias, this.company, this.street, this.postalCode, this.city, this.country);
    }



}
