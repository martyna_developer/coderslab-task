package stepDefinitions;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class Hooks {

    public static WebDriver driver;

    //a tu sobie ustalam warunki przed wykonaniem testu i po

    @Before
    public void beforeScenario()
    {
        System.setProperty("webdriver.chrome.driver", "/Users/martynawisnik/Downloads/chromedriver");
        WebDriverManager.chromedriver().setup();

        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("https://prod-kurs.coderslab.pl/index.php?controller=authentication");

    }

    @After
    public void afterScenario()
    {
        driver.quit();
    }

    public static void takeScreenshot() {

        File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);

        try {
            FileUtils.copyFile(scrFile, new File("src/test/screenshot/order.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
