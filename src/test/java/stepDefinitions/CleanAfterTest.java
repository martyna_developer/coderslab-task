package stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.AddresseesPage;

public class CleanAfterTest {

    private final WebDriver driver = Hooks.driver;
    private AddresseesPage addresseesPage;

    //tu sobie usuwam uzytkownika

    @When("Delete address")
    public void delete_address() {
        addresseesPage = new AddresseesPage(driver);
        addresseesPage.clickDeleteBtn();
    }

    @Then("Address is delete")
    public void address_is_delete() {
        addresseesPage.checkIfAddressIsDelete();
    }


}
