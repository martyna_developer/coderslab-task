package stepDefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.openqa.selenium.WebDriver;
import pages.CartPage;
import pages.HomePage;
import pages.ProductPage;

public class AddProductToCart {

    private WebDriver driver;
    private HomePage homePage;
    private ProductPage productPage;
    private CartPage cartPage;

    //tu sobie robie w gherkinie warunki dodawania produktow do koszyka

    @When("Add go product page {string}")
    public void add_go_product_page(String product) {
        this.driver = Hooks.driver;
        this.homePage = new HomePage(driver);
        this.productPage = this.homePage.goToProductPage(product);
    }
    @When("Choose size {string}")
    public void choose_size(String size) {
        this.productPage.chooseSize(size);
    }
    @When("Add {string} product items")
    public void add_product_items(String itemNmb) {
        this.productPage.addQuantity(itemNmb);
    }
    @When("Add product to cart")
    public void add_product_to_cart() {
        this.productPage.clickAddToCartBtn();
    }
    @When("Go to checkout option")
    public void go_to_checkout_option() {
       this.cartPage = this.productPage.clickCheckoutBtnOnPopup();
       this.cartPage.clickCheckoutBtn();
    }
    @When("Choose address")
    public void choose_address() {
        this.cartPage.chooseAddress();
    }
    @When("Choose pick up in store option")
    public void choose_pick_up_in_store_option() {
        this.cartPage.choosePickUpDeliveryOption();

    }
    @When("Choose pay by check option")
    public void choose_pay_by_check_option() {
        this.cartPage.choosePayByCheckOption();
    }
    @When("Order with an obligation to pay")
    public void order_with_an_obligation_to_pay() {
        this.cartPage.clickOrderWithAnObligationToPay();

    }
    @When("Make screenshot of order")
    public void make_screenshot_of_order() {
        Hooks.takeScreenshot();
    }

    @Then("Product is ordered correctly {string}, {string}, {string}")
    public void productIsOrderedCorrectly(String product, String size, String quantity) {

        this.cartPage.checkProductNameAndSize(product, size);
        this.cartPage.checkQuantity(quantity);
    }
}
